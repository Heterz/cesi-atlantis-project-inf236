import { Module } from '@nestjs/common';
import { PlanetModule } from './planets/planet.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    PlanetModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: [
        '.env.development.local',
        '.env.development',
        '.env',
        '.env.production',
      ],
    }),
  ],
})
export class AppModule {}
