import { Module } from '@nestjs/common';
import { PlanetController } from './planet.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Planet, PlanetSchema } from './planet.schema';
import { PlanetService } from './planet.service';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('MONGO_URI'),
        dbName: configService.get<string>('MONGO_DBNAME'),
        auth: {
          username: configService.get<string>('MONGO_USERNAME'),
          password: configService.get<string>('MONGO_PASSWORD'),
        },
      }),
    }),
    MongooseModule.forFeature([{ name: Planet.name, schema: PlanetSchema }]),
  ],
  controllers: [PlanetController],
  providers: [PlanetService],
})
export class PlanetModule {}
