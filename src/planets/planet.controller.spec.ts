import { PlanetController } from './planet.controller';
import { PlanetService } from './planet.service';
import { Planet } from './planet.schema';

describe('PlanetController', () => {
  let planetController: PlanetController;
  let planetService: PlanetService;

  beforeEach(async () => {
    planetService = new PlanetService(null);
    planetController = new PlanetController(planetService);
  });

  describe('root', () => {
    it('should return all planets', async () => {
      const result: Array<Planet> = [
        {
          name: 'Terre',
          description: 'Planète de la voie lactée',
          address: [9, 4, 23, 17, 32, 2, 1],
        },
      ];
      jest
        .spyOn(planetService, 'findAllPlanets')
        .mockImplementation(() => Promise.resolve(result));

      expect(await planetController.getPlanets()).toBe(result);
    });
  });
});
